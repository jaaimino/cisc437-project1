<?php
/* 
 * I want to recieve any type of request. 
 * This API will parrot back the VERB,
 * the PAYLOAD you sent,
 * and the REQUEST (api/:something/:else/:here will spit out [":something",":else",":here"])
 * as a json object
 */

require_once("dao.php");
require_once("requestHandler.php");

//$_SERVER is a super global, you should remember it from project 2.
$requestType = $_SERVER["REQUEST_METHOD"];


/*
 * $_GET and $_POST are the ways of getting the parameters in the standard case, but 
 * php://input is how you get the JSON data out of PUT, DELETE, PATCH, OPTIONS, etc.
 *
 * $_GET["key"] will spit out "value" if key and value were sent along.
 */
handleRequest($requestType);
?>
