Project 1


Specific guidelines

GET /customer should return an array of JSON objects having CustomerId, FirstName, LastName, and Email.
GET /customer/:CustomerId should return a JSON object with all of the information about a customer
GET /customer/:CustomerId/invoice should return an array of JSON objects having InvoiceId, InvoiceDate, and Total
GET /customer/:CustomerId/invoice/:InvoiceId should return a JSON object having all information about that invoice plus the key InvoiceLines with value an array of JSON objects having TrackId, (track) Name, (album) Title, (artist) Name, (genre) Name, UnitPrice, and Quantity
POST /customer/ should take in data about a new Customer, create the customer, and return the same as GET /customer/:CustomerId, most importantly the new CustomerId
PUT /customer/:CustomerId should update a customer using the given data
DELETE /customer/:CustomerId should delete the customer
GET /playlist should return an array of JSON objects with PlaylistId and (playlist)Name.
GET /playlist/:PlaylistId should return an array of JSON objects, each with information about the appropriate tracks from that playlist (you can pick, but enough for a web-app running from this data to show interesting things about each track).
GET /revenue should return an array of JSON objects each providing the revenue of the store in a given month
GET /revenue[/:year[/:month[/:day]]] should return a single JSON object giving the revenue for the requested period