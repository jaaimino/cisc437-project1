<?php
/**
 * Determine url route and return relevant information from DAO
 */
$response = new StdClass();

function handleRequest($requestType){
	global $response;
	$response->requestType = $requestType;
	$response->request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

	switch ($requestType) {
		case 'GET':
		handleGET();
	    break;
	    case 'POST':
	    handlePOST();
	    break;
        case 'PUT':
        handlePUT();
        break;
        case 'DELETE':
        handleDELETE();
        break;
	    default:
	    parse_str(file_get_contents("php://input"), $response->payload);
	    echo json_encode($response);
	    break;
	};
}

/**
 *Figure out what the caller wants, and fetch it from the database
 **/
function handleGET(){
	global $response;
	$response->payload = $_GET;
	//Get all customers
    if(sizeof($response->request) == 1 && $response->request[0] == "customer") {
    	$dao = new DAO();
    	echo $dao->getAllCustomers();
    	return;
    }
    //Get customer info by id
    if(sizeof($response->request) == 2 && $response->request[0] == "customer") {
    	$dao = new DAO();
    	$id = ((int)$response->request[1]);
    	echo $dao->getCustomerById($id);
    	return;
    }
    //Get invoices of a customer
    if(sizeof($response->request) == 3 && $response->request[0] == "customer" && $response->request[2] == "invoice") {
    	$dao = new DAO();
    	$id = ((int)$response->request[1]);
    	echo $dao->getCustomerIdInvoices($id);
    	return;
    }
    //Get invoices of a customer
    if(sizeof($response->request) == 4 && $response->request[0] == "customer" && $response->request[2] == "invoice") {
    	$dao = new DAO();
    	$id = ((int)$response->request[1]);
    	$invoiceId = ((int)$response->request[3]);
    	echo $dao->getCustomerIdInvoiceId($id, $invoiceId);
    	return;
    }
    //Get all playlists
    if(sizeof($response->request) == 1 && $response->request[0] == "playlist") {
    	$dao = new DAO();
    	echo $dao->getAllPlaylists();
    	return;
    }
    //Get single playlist info
    if(sizeof($response->request) == 2 && $response->request[0] == "playlist") {
    	$dao = new DAO();
    	$id = ((int)$response->request[1]);
    	echo $dao->getPlaylistById($id);
    	return;
    }
    //Get revenue for all months
    if(sizeof($response->request) == 1 && $response->request[0] == "revenue") {
        $dao = new DAO();
        echo $dao->getAllRevenue();
        return;
    }
    //Get revenue for a year  (Must match sqlite date format ex. 04 for month)
    if(sizeof($response->request) == 2 && $response->request[0] == "revenue") {
        $dao = new DAO();
        $year = $response->request[1];
        echo $dao->getRevenueForYear($year);
        return;
    }
    //Get revenue for a year and a month (Must match sqlite date format ex. 04 for month)
    if(sizeof($response->request) == 3 && $response->request[0] == "revenue") {
        $dao = new DAO();
        $year = $response->request[1];
        $month = $response->request[2];
        echo $dao->getRevenueForYearAndMonth($year, $month);
        return;
    }
    //Get revenue for a year, month, and day  (Must match sqlite date format ex. 04 for month)
    if(sizeof($response->request) == 4 && $response->request[0] == "revenue") {
        $dao = new DAO();
        $year = $response->request[1];
        $month = $response->request[2];
        $day = $response->request[3];
        echo $dao->getRevenueForYearAndMonthAndDay($year, $month, $day);
        return;
    }
    echo "Bad request..";
}

//I expect the data to come in as form data
function handlePOST(){
	global $response;
	$response->payload = $_POST;
    if(sizeof($response->request) == 2 && $response->request[0] == "customer" && $response->request[1] == "") {
        $dao = new DAO();
        $FirstName = (isset($_POST["FirstName"]) ? $_POST["FirstName"] : NULL);
        $LastName = (isset($_POST["LastName"]) ? $_POST["LastName"] : NULL);
        $Company = (isset($_POST["Company"]) ? $_POST["Company"] : NULL);
        $Address = (isset($_POST["Address"]) ? $_POST["Address"] : NULL);
        $City = (isset($_POST["City"]) ? $_POST["City"] : NULL);
        $State = (isset($_POST["State"]) ? $_POST["State"] : NULL);
        $Country = (isset($_POST["Country"]) ? $_POST["Country"] : NULL);
        $PostalCode = (isset($_POST["PostalCode"]) ? $_POST["PostalCode"] : NULL);
        $Phone = (isset($_POST["Phone"]) ? $_POST["Phone"] : NULL);
        $Fax = (isset($_POST["Fax"]) ? $_POST["Fax"] : NULL);
        $Email = (isset($_POST["Email"]) ? $_POST["Email"] : NULL);
        $SupportRepId = (isset($_POST["SupportRepId"]) ? $_POST["SupportRepId"] : NULL);
        echo $dao->insertNewCustomer(
            $FirstName,
            $LastName,
            $Company,
            $Address, 
            $City, 
            $State, 
            $Country, 
            $PostalCode, 
            $Phone, 
            $Fax, 
            $Email, 
            $SupportRepId);
        return;
    }
	echo "Bad post..";
}

//This data will come in as raw json
function handlePUT(){
    global $response;
    if(sizeof($response->request) == 2 && $response->request[0] == "customer") {
        $_PUT = json_decode(file_get_contents("php://input"));
        $dao = new DAO();
        $id = ((int)$response->request[1]);
        $FirstName = (isset($_PUT->FirstName) ? $_PUT->FirstName : NULL);
        $LastName = (isset($_PUT->LastName) ? $_PUT->LastName : NULL);
        $Company = (isset($_PUT->Company) ? $_PUT->Company : NULL);
        $Address = (isset($_PUT->Address) ? $_PUT->Address : NULL);
        $City = (isset($_PUT->City) ? $_PUT->City : NULL);
        $State = (isset($_PUT->State) ? $_PUT->State : NULL);
        $Country = (isset($_PUT->Country) ? $_PUT->Country : NULL);
        $PostalCode = (isset($_PUT->PostalCode) ? $_PUT->PostalCode : NULL);
        $Phone = (isset($_PUT->Phone) ? $_PUT->Phone : NULL);
        $Fax = (isset($_PUT->Fax) ? $_PUT->Fax : NULL);
        $Email = (isset($_PUT->Email) ? $_PUT->Email : NULL);
        $SupportRepId = (isset($_PUT->SupportRepId) ? $_PUT->SupportRepId : NULL);
        echo $dao->updateCustomerById(
            $id,
            $FirstName,
            $LastName,
            $Company,
            $Address, 
            $City, 
            $State, 
            $Country, 
            $PostalCode, 
            $Phone, 
            $Fax, 
            $Email, 
            $SupportRepId);
        return;
    }
}

function handleDELETE(){
    global $response;
    //$response->payload = $_DELETE;
    if(sizeof($response->request) == 2 && $response->request[0] == "customer") {
        $id = ((int)$response->request[1]);
        $dao = new DAO();
        echo $dao->deleteCustomerById($id);
        return;
    }
    echo "Bad delete";
}

?>