<?php
/**
 * Quick database handler class for database things
 */
class DAO{
	private $DB_PATH = "Chinook_Sqlite.sqlite";
	private $lastId;
	private function open_database(){
		global $DB_PATH;
		$dbhandle = new PDO("sqlite:".$this->DB_PATH) or die("Failed to open DB");
	      if (!$dbhandle)
	      	die ($error);
	    return $dbhandle;
	}

	public function getAllCustomers(){
		$dbhandle = $this->open_database();
		$query = "SELECT CustomerId, FirstName, LastName, Email FROM Customer";

		$statement = $dbhandle->prepare($query);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return json_encode($results);
	}

	public function getCustomerById($id){
		$dbhandle = $this->open_database();
		$query = "SELECT * FROM Customer WHERE CustomerId=:id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(isset($results[0])){
			return json_encode($results[0]);
		}
	}

	//Get all customer invoices by customer id
	public function getCustomerIdInvoices($id){
		$dbhandle = $this->open_database();
		$query = "SELECT i.InvoiceId, i.InvoiceDate, i.Total FROM Invoice AS i INNER JOIN Customer AS cu ON i.CustomerId = cu.CustomerId WHERE cu.CustomerId = :id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		return json_encode($results);
	}

	//Get customer by id and invoice by id with some extras. @TODO
	public function getCustomerIdInvoiceId($id, $invoiceId){
		$dbhandle = $this->open_database();
		$query = "SELECT * FROM Customer WHERE CustomerId=:id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		//Find invoices with matching customer id
		$query = "SELECT * FROM Invoice WHERE InvoiceId=:invoiceId";

		$statement = $dbhandle->prepare($query);
		//$statement->bindValue(':id', $results[0]['CustomerId']);
		$statement->bindValue(':invoiceId', $invoiceId);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		//Now to get invoicelines
		$query = "SELECT * FROM Customer WHERE CustomerId=:id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);

		//Find invoices with matching customer id
		$query = "SELECT tr.TrackId, tr.Name AS Track, al.Title AS Album, ar.Name AS Artist, gr.Name AS Genre, tr.UnitPrice, il.Quantity FROM InvoiceLine AS il " . 
		"INNER JOIN Track AS tr ON il.TrackId = tr.TrackId " . 
		"INNER JOIN Genre AS gr ON gr.GenreId = tr.GenreId " . 
		"INNER JOIN Album AS al ON tr.AlbumId = al.AlbumId " . 
		"INNER JOIN Artist AS ar ON al.ArtistId = ar.ArtistId " .
		"WHERE il.InvoiceId = :id";

		$statement = $dbhandle->prepare($query);
		//$statement->bindValue(':id', $results[0]['CustomerId']);
		$statement->bindValue(':id', $invoiceId);
		$statement->execute();
		if(isset($results[0])){
			$results[0]["InvoiceLines"] = $statement->fetchAll(PDO::FETCH_ASSOC);
			return json_encode($results[0]);
		}
	}

	//Get all playlists
	public function getAllPlaylists(){
		$dbhandle = $this->open_database();
		$query = "SELECT * FROM Playlist";

		$statement = $dbhandle->prepare($query);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return json_encode($results);
	}

	//Get single playlist info
	public function getPlaylistById($id){
		$dbhandle = $this->open_database();
		$query = "SELECT tr.TrackId, tr.Name AS Track, al.Title AS Album, ar.Name AS Artist, gr.Name AS Genre FROM PlaylistTrack AS pt " . 
		"INNER JOIN Track AS tr ON pt.TrackId = tr.TrackId " . 
		"INNER JOIN Genre AS gr ON gr.GenreId = tr.GenreId " . 
		"INNER JOIN Album AS al ON tr.AlbumId = al.AlbumId " . 
		"INNER JOIN Artist AS ar ON al.ArtistId = ar.ArtistId " .
		"WHERE pt.PlayListId = :id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC); //All playlist tracks with matching playlist id
		echo json_encode($results);
	}

	//Get all revenue
	public function getAllRevenue(){
		$dbhandle = $this->open_database();
		$query = "SELECT strftime('%m', InvoiceDate) AS Month, TOTAL(Total) AS Total FROM Invoice GROUP BY strftime('%m', InvoiceDate);";

		$statement = $dbhandle->prepare($query);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		$results2 = array();
		foreach($results as $test){
			for($i=0;$i<=sizeof($results)-1;$i++){
				$results2[$i] = array($results[$i]["Month"] => $results[$i]["Total"]);
			}
		}
		
		return json_encode($results2);
	}

	//Get revenue for a year
	public function getRevenueForYear($year){
		$dbhandle = $this->open_database();
		$query = "SELECT TOTAL(Total) AS Total FROM Invoice WHERE strftime('%Y', InvoiceDate) = ?;";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(1, $year, PDO::PARAM_STR);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return json_encode($results);
	}

	//Get revenue for a year and month
	public function getRevenueForYearAndMonth($year, $month){
		$dbhandle = $this->open_database();
		$query = "SELECT TOTAL(Total) AS Total FROM Invoice WHERE strftime('%Y', InvoiceDate) = ? " . 
		"AND strftime('%m', InvoiceDate) = ?;";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(1, $year);
		$statement->bindValue(2, $month);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return json_encode($results);
	}

	//Get revenue for a year, month, and day
	public function getRevenueForYearAndMonthAndDay($year, $month, $day){
		$dbhandle = $this->open_database();
		$query = "SELECT TOTAL(Total) AS Total FROM Invoice WHERE strftime('%Y', InvoiceDate) = ? " . 
		"AND strftime('%m', InvoiceDate) = ? AND strftime('%d', InvoiceDate) = ?;"; //Month

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(1, $year);
		$statement->bindValue(2, $month);
		$statement->bindValue(3, $day);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return json_encode($results);
	}

	//Insert new customer. ALL VALUES ARE REQUIRED. Sorry :(
	public function insertNewCustomer($FirstName, $LastName, $Company, $Address, $City, $State, 
		$Country, $PostalCode, $Phone, $Fax, $Email, $SupportRepId){
		$dbhandle = $this->open_database();
		$query = "INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, Phone, Fax, Email, SupportRepId)" . 
		"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(1, $FirstName);
		$statement->bindValue(2, $LastName);
		$statement->bindValue(3, $Company);
		$statement->bindValue(4, $Address);
		$statement->bindValue(5, $City);
		$statement->bindValue(6, $State);
		$statement->bindValue(7, $Country);
		$statement->bindValue(8, $PostalCode);
		$statement->bindValue(9, $Phone);
		$statement->bindValue(10, $Fax);
		$statement->bindValue(11, $Email);
		$statement->bindValue(12, $SupportRepId);
		$statement->execute();

		//Grab last insert id and send data back as json
		$id = $dbhandle->lastInsertId();

		$query = "SELECT * FROM Customer WHERE CustomerId=:id;";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->execute();

		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(isset($results[0])){
			return json_encode($results[0]);
		}
	}

	//Update a customer by id. REQUIRES ALL FIELDS. SORRY!
	public function updateCustomerById($id, $FirstName, $LastName, $Company, $Address, $City, $State,
		$Country, $PostalCode, $Phone, $Fax, $Email, $SupportRepId){

		$dbhandle = $this->open_database();
		$query = "UPDATE Customer SET FirstName = ?, LastName = ?, Company = ?, Address = ?, City = ?, State = ?, Country = ?" . 
		", PostalCode = ?, Phone = ?, Fax = ?, Email = ?, SupportRepId = ? WHERE CustomerId = :id;";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$statement->bindValue(1, $FirstName);
		$statement->bindValue(2, $LastName);
		$statement->bindValue(3, $Company);
		$statement->bindValue(4, $Address);
		$statement->bindValue(5, $City);
		$statement->bindValue(6, $State);
		$statement->bindValue(7, $Country);
		$statement->bindValue(8, $PostalCode);
		$statement->bindValue(9, $Phone);
		$statement->bindValue(10, $Fax);
		$statement->bindValue(11, $Email);
		$statement->bindValue(12, $SupportRepId);
		$statement->execute();
		
		return "Put request complete";
	}

	public function deleteCustomerById($id){
		$dbhandle = $this->open_database();
		$query = "DELETE FROM Customer WHERE CustomerId=:id";

		$statement = $dbhandle->prepare($query);
		$statement->bindValue(':id', $id);
		$success = $statement->execute();
		return "Delete was executed!";
	}
}

?>